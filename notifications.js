const none = `
<li class="nav-item" id="stat-link">
    <img src="/img/no-update.svg" class="notification">
</li>
`
const nonedark = `
<li class="nav-item" id="stat-link">
    <img src="/img/no-update-dark.svg" class="notification">
</li>
`

const oneplus = `
<li class="nav-item" id="stat-link">
    <img src="/img/update.svg" class="notification">
</li>
`
const oneplusdark = `
<li class="nav-item" id="stat-link">
    <img src="/img/update-dark.svg" class="notification">
</li>
`

var keyCode = "XNULLX";

db.ref("Notifications/current").get().then((snap) => {
    if(snap.exists()) {
        if(snap.val() != localStorage.getItem("CurrentKeyCode")) {
            if(localStorage.getItem("theme") == "dark") {
                document.getElementById("stat-link").innerHTML = oneplusdark
            } else {
                document.getElementById("stat-link").innerHTML = oneplus;
            }
            localStorage.setItem(snap.val())
        }else {
            if(localStorage.getItem("theme") == "dark") {
                document.getElementById("stat-link").innerHTML = nonedark
            } else {
                document.getElementById("stat-link").innerHTML = none;
            }
        }
    }
    else {
        document.getElementById("stat-link").innerHTML = none;
    }
})