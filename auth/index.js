function xxLG(email, password) {
    auth.signInWithEmailAndPassword(email, password)
    .then((userCredential) => {
        localStorage.setItem("__x5__", email)
        localStorage.setItem("__x52__", btoa(password))
        window.location.reload()
    })
    .catch((error) => {
        console.log(error)
    })
}

function isLoggedIn() {
    if(localStorage.getItem("__x5__") != null && localStorage.getItem("__x52__") != null) {
        return true;
    }
    else {
        return false;
    }
}